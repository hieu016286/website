@extends('layouts.template')

@section('content')
    <div class="container-fluid">
        <div class="row pt-3 w-100">
            <div class="col d-flex">
                <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" fill="currentColor"
                     class="bi bi-box-arrow-right mt-2 text-success" viewBox="0 0 16 16">
                    <path fill-rule="evenodd"
                          d="M10 12.5a.5.5 0 0 1-.5.5h-8a.5.5 0 0 1-.5-.5v-9a.5.5 0 0 1 .5-.5h8a.5.5 0 0 1 .5.5v2a.5.5 0 0 0 1 0v-2A1.5 1.5 0 0 0 9.5 2h-8A1.5 1.5 0 0 0 0 3.5v9A1.5 1.5 0 0 0 1.5 14h8a1.5 1.5 0 0 0 1.5-1.5v-2a.5.5 0 0 0-1 0v2z"/>
                    <path fill-rule="evenodd"
                          d="M15.854 8.354a.5.5 0 0 0 0-.708l-3-3a.5.5 0 0 0-.708.708L14.293 7.5H5.5a.5.5 0 0 0 0 1h8.793l-2.147 2.146a.5.5 0 0 0 .708.708l3-3z"/>
                </svg>&ensp;
                <h1 class="font-weight-bold">Role Detail</h1>
            </div>
        </div>
        <div class="row justify-content-between">
            <div class="container">
                <div class="row mb-3 mt-5">
                    <div class="card w-100">
                        <div class="card-header">
                            <ul class="nav nav-pills">
                                <li class="nav-item"><a class="nav-link active bg-info">Role Detail
                                        - {{ $role->id }}</a></li>
                            </ul>
                        </div><!-- /.card-header -->
                        <div class="card-body">
                            <h5 class="card-title">
                                <span class="font-weight-bold">Display Name :</span>&nbsp;&nbsp;
                                <span class="badge bg-success">{{ $role->display_name }}</span>
                            </h5>
                            <br><br>
                            <p class="card-text">
                                User Count : {{ $role->users()->count() }}
                            </p>
                            <h5>Permission</h5>

                            @foreach($role->permissions as $permission)
                                <span class="badge badge-info">{{$permission->display_name}}</span>
                            @endforeach
                            <br>
                            <br>
                            <a href="{{ route('roles.edit',$role->id) }}" class="btn btn-warning">
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                                     class="bi bi-pencil" viewBox="0 0 16 16">
                                    <path
                                        d="M12.146.146a.5.5 0 0 1 .708 0l3 3a.5.5 0 0 1 0 .708l-10 10a.5.5 0 0 1-.168.11l-5 2a.5.5 0 0 1-.65-.65l2-5a.5.5 0 0 1 .11-.168l10-10zM11.207 2.5 13.5 4.793 14.793 3.5 12.5 1.207 11.207 2.5zm1.586 3L10.5 3.207 4 9.707V10h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.293l6.5-6.5zm-9.761 5.175-.106.106-1.528 3.821 3.821-1.528.106-.106A.5.5 0 0 1 5 12.5V12h-.5a.5.5 0 0 1-.5-.5V11h-.5a.5.5 0 0 1-.468-.325z"/>
                                </svg>
                            </a>
                            <a href="{{ route('roles.index') }}" class="btn btn-primary float-right"><i
                                    class="fas fa-home"></i>&nbsp;&nbsp;Home</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
