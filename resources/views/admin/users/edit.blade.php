@extends('layouts.template')

@section('content')
    <div class="container-fluid">
        <div class="row pt-3 w-100">
            <div class="col d-flex">
                <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" fill="currentColor"
                     class="bi bi-box-arrow-right mt-2 text-success" viewBox="0 0 16 16">
                    <path fill-rule="evenodd"
                          d="M10 12.5a.5.5 0 0 1-.5.5h-8a.5.5 0 0 1-.5-.5v-9a.5.5 0 0 1 .5-.5h8a.5.5 0 0 1 .5.5v2a.5.5 0 0 0 1 0v-2A1.5 1.5 0 0 0 9.5 2h-8A1.5 1.5 0 0 0 0 3.5v9A1.5 1.5 0 0 0 1.5 14h8a1.5 1.5 0 0 0 1.5-1.5v-2a.5.5 0 0 0-1 0v2z"/>
                    <path fill-rule="evenodd"
                          d="M15.854 8.354a.5.5 0 0 0 0-.708l-3-3a.5.5 0 0 0-.708.708L14.293 7.5H5.5a.5.5 0 0 0 0 1h8.793l-2.147 2.146a.5.5 0 0 0 .708.708l3-3z"/>
                </svg>&ensp;
                <h1>Edit User</h1>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-md-8 mt-5">
                <div class="card">
                    <div class="card-header d-flex justify-content-between">
                        <p class="font-weight-bold mb-0">Edit User - {{$user->id}}</p>
                    </div>
                    <div class="card-body">
                        <form action="{{ route('users.update', $user->id) }}" method="post">
                            @csrf
                            @method('put')
                            <div class="form-group">
                                <label for="name">Name :</label>
                                <input type="text" class="form-control" name="name" placeholder="Name"
                                       value="{{$user->name}}">
                                @if($errors->has('name'))
                                    <p class="text-danger">{{$errors->first('name')}}</p>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="display_name">Email:</label>
                                <input type="email" class="form-control" name="email" placeholder="Email"
                                       value="{{$user->email}}">
                                @if($errors->has('email'))
                                    <p class="text-danger">{{$errors->first('email')}}</p>
                                @endif
                            </div>
                            <div>
                                @foreach($roles as $role)
                                    <div class="form-check">
                                        <input class="form-check-input"
                                               type="checkbox" value="{{ $role->id }}"
                                               @if(in_array($role->id,$user->roles()->pluck('id')->toArray())) checked
                                               @endif
                                               name="role_ids[]"
                                        >
                                        <label class="form-check-label">
                                            {{ $role->display_name }}
                                        </label>
                                    </div>
                                @endforeach
                            </div>
                            <br>
                            <button type="submit" class="btn btn-success">Update</button>
                            <a href="{{ route('users.index') }}" class="btn btn-primary float-right"><i
                                    class="fas fa-home"></i>&nbsp;&nbsp;Home</a>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
