@extends('layouts.template')

@section('content')
    <!-- Modal Create -->
    <div class="modal fade" id="CreateCategoryModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Create Category</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group mb-3">
                        <label for="">Name :</label>
                        <input type="text" class="form-control" id="nameCreate">
                        <span class="text-danger name_error_create"></span>
                    </div>
                    <div class="form-group mb-3">
                        <label for="">Main Category :</label>
                        <select class="custom-select" id="parentIdCreate">
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary closeForm" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary createCategory"
                            data-action="{{ route('categories.store') }}">Save
                    </button>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal Edit -->
    <div class="modal fade" id="EditCategoryModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit Category</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <input type="hidden" id="cID">
                    <div class="form-group mb-3">
                        <label for="">Name :</label>
                        <input type="text" class="name form-control" id="nameUpdate">
                        <span class="text-danger name_error_update"></span>
                    </div>
                    <div class="form-group mb-3">
                        <label for="">Main Category :</label>
                        <select class="custom-select" id="parentIdUpdate">
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary closeForm" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary updateCategory">Update</button>
                </div>
            </div>
        </div>
    </div>

    <div class="row pt-3 w-100">
        <div class="col d-flex">
            <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" fill="currentColor"
                 class="bi bi-box-arrow-right mt-2 text-success" viewBox="0 0 16 16">
                <path fill-rule="evenodd"
                      d="M10 12.5a.5.5 0 0 1-.5.5h-8a.5.5 0 0 1-.5-.5v-9a.5.5 0 0 1 .5-.5h8a.5.5 0 0 1 .5.5v2a.5.5 0 0 0 1 0v-2A1.5 1.5 0 0 0 9.5 2h-8A1.5 1.5 0 0 0 0 3.5v9A1.5 1.5 0 0 0 1.5 14h8a1.5 1.5 0 0 0 1.5-1.5v-2a.5.5 0 0 0-1 0v2z"/>
                <path fill-rule="evenodd"
                      d="M15.854 8.354a.5.5 0 0 0 0-.708l-3-3a.5.5 0 0 0-.708.708L14.293 7.5H5.5a.5.5 0 0 0 0 1h8.793l-2.147 2.146a.5.5 0 0 0 .708.708l3-3z"/>
            </svg>&ensp;
            <h1>All Categories</h1>
        </div>
    </div>
    <div class="row justify-content-between">
        <div class="container">
            <div class="row d-flex justify-content-center">
                <div class="col-md-10">
                    <form action="" class="card p-3 py-4 mt-3" method="get">
                        <div class="row g-3 mt-2">
                            <div class="col-md-3">
                                <select class="form-select form-control" aria-label="Default select example"
                                        id="parentId" data-action="{{route('categories.getParentCategories')}}">

                                </select>
                                <input type="hidden" value=" " id="currentParentId">
                            </div>
                            <div class="col-md-6"><input type="text" class="form-control" placeholder="Enter Name ..."
                                                         id="name"></div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid py-5">
        <div class="row">
            <div class="col-md-12">
                <div id="success_message">
                </div>
                <div class="card">
                    <div class="card-header">
                        <h4>
                            @hasPermission('create-category')
                            <a href="" data-toggle="modal" data-target="#CreateCategoryModal"
                               class="btn btn-primary float-right btn-sm">Create Category</a>
                            @endhasPermission
                        </h4>
                    </div>
                    <div class="card-body">
                        <div id="list" data-action="{{route('categories.list')}}">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
    <script src="{{ asset('admin/js/category.js') }}"></script>
@endpush
