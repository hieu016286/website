@extends('layouts.template')

@section('content')
    <div class="container-fluid">
        <div class="row pt-3 w-100">
            <div class="col d-flex">
                <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" fill="currentColor"
                     class="bi bi-box-arrow-right mt-2 text-success" viewBox="0 0 16 16">
                    <path fill-rule="evenodd"
                          d="M10 12.5a.5.5 0 0 1-.5.5h-8a.5.5 0 0 1-.5-.5v-9a.5.5 0 0 1 .5-.5h8a.5.5 0 0 1 .5.5v2a.5.5 0 0 0 1 0v-2A1.5 1.5 0 0 0 9.5 2h-8A1.5 1.5 0 0 0 0 3.5v9A1.5 1.5 0 0 0 1.5 14h8a1.5 1.5 0 0 0 1.5-1.5v-2a.5.5 0 0 0-1 0v2z"/>
                    <path fill-rule="evenodd"
                          d="M15.854 8.354a.5.5 0 0 0 0-.708l-3-3a.5.5 0 0 0-.708.708L14.293 7.5H5.5a.5.5 0 0 0 0 1h8.793l-2.147 2.146a.5.5 0 0 0 .708.708l3-3z"/>
                </svg>&ensp;
                <h1>Edit Product</h1>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-md-8 mt-5">
                <div class="card">
                    <div class="card-header d-flex justify-content-between">
                        <p class="font-weight-bold mb-0">Edit Product</p>
                    </div>
                    <div class="card-body">
                        <form action="{{route('products.update',$product->id)}}" method="post"
                              enctype="multipart/form-data">
                            @csrf
                            @method('PUT')
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="name">Name :</label>
                                        <input type="text" class="form-control" name="name" placeholder="Name"
                                               value="{{$product->name}}">
                                        @if($errors->has('name'))
                                            <p class="text-danger">{{$errors->first('name')}}</p>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="price"> Price :</label>
                                        <input type="number" class="form-control" name="price" placeholder="price"
                                               value="{{$product->price}}">
                                        @if($errors->has('price'))
                                            <p class="text-danger">{{$errors->first('price')}}</p>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <select class="select2 form-control " name="category_ids[]" multiple="multiple">
                                    @foreach($categories as $category)
                                        <option value="{{$category->id}}"
                                                @foreach($product->categories as $p) @if($category->id == $p->id)selected="selected"@endif @endforeach>{{$category->name}}</option>
                                    @endforeach
                                    @if($errors->has('category_ids'))
                                        <p class="text-danger">{{$errors->first('category_ids')}}</p>
                                    @endif
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="image"> Image :</label>
                                <input type="file" class="form-control" name="image" placeholder="image" id="image">
                                @if ($errors->has('image'))
                                    <strong>{{ $errors->first('image') }}</strong>
                                @endif
                            </div>
                            <div class="form-group">
                                <img id="showImage"
                                     src="{{ !empty($product->image) ? url('images/'.$product->image) : "" }}"
                                     alt=""
                                     style="width: 300px; height: 300px; border: 1px solid #000;">
                            </div>
                            <br>
                            <div class="form-group">
                                <label for="description">Description :</label>
                                <textarea type="text" class="form-control" name="description">
                                    {{$product->description}}
                                    @if($errors->has('description'))
                                        <p class="text-danger">{{$errors->first('description')}}</p>
                                    @endif
                                </textarea>
                            </div>
                            <br>
                            <button type="submit" class="btn btn-success mt-3">Update</button>
                            <a href="{{ route('products.index') }}" class="btn btn-primary float-right mt-3"><i
                                    class="fas fa-home"></i>&nbsp;&nbsp;Home</a>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script>tinymce.init({
            selector: 'textarea',
            height: 500,
        });
    </script>
    <script>
        $(document).ready(function () {
            $('.select2').select2();
        });
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#image').change(function (e) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#showImage').attr('src', e.target.result);
                }
                reader.readAsDataURL(e.target.files['0']);
            })
        })
    </script>
@endpush
