<?php

namespace App\Http\Controllers;

use App\Services\CategoryService;
use App\Services\ProductService;
use App\Services\RoleService;
use App\Services\UserService;

class HomeController extends Controller
{
    protected $userService;
    protected $roleService;
    protected $productService;
    protected $categoryService;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(
        UserService     $userService,
        RoleService     $roleService,
        ProductService  $productService,
        CategoryService $categoryService
    )
    {
        $this->middleware('auth');
        $this->userService = $userService;
        $this->roleService = $roleService;
        $this->productService = $productService;
        $this->categoryService = $categoryService;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $users = $this->userService->count();
        $roles = $this->roleService->count();
        $products = $this->productService->count();
        $categories = $this->categoryService->count();
        return view('admin.home', compact('users', 'roles', 'products', 'categories'));
    }
}
