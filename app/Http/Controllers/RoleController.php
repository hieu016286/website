<?php

namespace App\Http\Controllers;

use App\Http\Requests\Role\RoleRequest;
use App\Services\PermissionService;
use Illuminate\Support\Facades\View;
use Illuminate\Http\Request;
use App\Services\RoleService;

class RoleController extends Controller
{
    protected $roleService;
    protected $permissionService;

    public function __construct(RoleService $roleService, PermissionService $permissionService)
    {
        $this->roleService = $roleService;
        $this->permissionService = $permissionService;
        View::share('permissions', $this->permissionService->getPermissions());
    }

    public function index(Request $request)
    {
        $roles = $this->roleService->search($request);
        return view('admin.roles.index', compact('roles', 'request'));
    }

    public function create()
    {
        return view('admin.roles.create');
    }

    public function store(RoleRequest $request)
    {
        $this->roleService->create($request);
        return redirect()->route('roles.index')->with('success', 'Create Role Success !!!');
    }

    public function show($id)
    {
        $role = $this->roleService->findById($id);
        return view('admin.roles.show', compact('role'));
    }

    public function edit($id)
    {
        $role = $this->roleService->findById($id);
        return view('admin.roles.edit', compact('role'));
    }

    public function update(RoleRequest $request, $id)
    {
        $this->roleService->update($request, $id);
        return redirect()->route('roles.index')->with('update', 'Update Role Success !!!');
    }

    public function destroy($id)
    {
        $this->roleService->delete($id);
        return redirect()->route('roles.index');
    }
}
