<?php

namespace App\Http\Controllers;

use App\Http\Requests\Product\ProductRequest;
use App\Services\CategoryService;
use App\Services\ProductService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;

class ProductController extends Controller
{
    protected $productService;
    protected $categoryService;

    public function __construct(ProductService $productService, CategoryService $categoryService)
    {
        $this->productService = $productService;
        $this->categoryService = $categoryService;
        View::share('categories', $this->categoryService->getCategories());
    }

    public function index(Request $request)
    {
        $products = $this->productService->search($request);
        return view('admin.products.index', compact('products', 'request'));
    }

    public function create()
    {
        return view('admin.products.create');
    }

    public function store(ProductRequest $request)
    {
        $this->productService->create($request);
        return redirect()->route('products.index')->with('success', 'Create Product Success !!!');
    }

    public function edit($id)
    {
        $product = $this->productService->findById($id);
        return view('admin.products.edit', compact('product'));
    }

    public function show($id)
    {
        $product = $this->productService->findById($id);
        return view('admin.products.show', compact('product'));
    }

    public function update(ProductRequest $request, $id)
    {
        $this->productService->update($request, $id);
        return redirect()->route('products.index')->with('update', 'Update Product Success !!!');
    }

    public function destroy($id)
    {
        $this->productService->delete($id);
        return redirect()->route('products.index');
    }
}
