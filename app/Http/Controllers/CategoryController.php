<?php

namespace App\Http\Controllers;

use App\Http\Requests\Category\CategoryRequest;
use App\Services\CategoryService;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    protected $categoryService;

    public function __construct(CategoryService $categoryService)
    {
        $this->categoryService = $categoryService;
    }

    public function index()
    {
        return view('admin.categories.index');
    }

    public function list(Request $request)
    {
        $categories = $this->categoryService->search($request);
        return view('admin.categories.list', compact('categories'))->render();
    }

    public function getParentCategories()
    {
        $parentCategories = $this->categoryService->getParentCategories();
        return response()->json([
            'parentCategories' => $parentCategories,
        ]);
    }

    public function store(CategoryRequest $request)
    {
        $this->categoryService->create($request);
        return response()->json([
            'message' => 'Create Category Success !!!',
        ]);
    }

    public function edit($id)
    {
        $category = $this->categoryService->findById($id);
        $parentCategories = $this->categoryService->getParentCategories();
        return response()->json([
            'category' => $category,
            'parentCategories' => $parentCategories,
        ]);
    }

    public function update(CategoryRequest $request, $id)
    {
        $this->categoryService->update($request, $id);
        return response()->json([
            'message' => 'Update Category Success !!!',
        ]);
    }

    public function destroy($id)
    {
        return $this->categoryService->delete($id);
    }
}
