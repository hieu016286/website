<?php

namespace App\Services;

use App\Http\Resources\CategoryCollection;
use App\Repositories\CategoryRepository;

class CategoryService
{

    protected $categoryRepository;

    public function __construct(CategoryRepository $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
    }

    public function count()
    {
        return $this->categoryRepository->count();
    }

    public function search($request)
    {
        $dataSearch = $request->all();
        $dataSearch['name'] = $request->name ?? '';
        $dataSearch['parent_id'] = $request->parent_id ?? '';
        return $this->categoryRepository->search($dataSearch);
    }

    public function getParentCategories()
    {
        $parentCategories = $this->categoryRepository->getParentCategories();
        return new CategoryCollection($parentCategories);
    }

    public function getCategories()
    {
        return $this->categoryRepository->getCategories();
    }

    public function create($request)
    {
        $dataCreate = $request->all();
        return $this->categoryRepository->create($dataCreate);
    }

    public function findById($id)
    {
        return $this->categoryRepository->findById($id);
    }

    public function update($request, $id)
    {
        $dataUpdate = $request->all();
        $category = $this->categoryRepository->findById($id);
        $category->update($dataUpdate);
        return $category;
    }

    public function delete($id)
    {
        $category = $this->categoryRepository->findById($id);
        $category->delete();
        return $category;
    }
}
