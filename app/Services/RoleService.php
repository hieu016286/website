<?php

namespace App\Services;

use App\Repositories\RoleRepository;

class RoleService
{
    protected $roleRepository;

    public function __construct(RoleRepository $roleRepository)
    {
        $this->roleRepository = $roleRepository;
    }

    public function count()
    {
        return $this->roleRepository->count();
    }

    public function search($request)
    {
        $dataSearch = $request->all();
        $dataSearch['name'] = $request->name ?? '';
        return $this->roleRepository->search($dataSearch)->appends($request->all());
    }

    public function getRoleWithOutSuperAdmin()
    {
        return $this->roleRepository->getRoleWithOutSuperAdmin();
    }

    public function create($request)
    {
        $dataCreate = $request->all();
        $dataCreate['permission_ids'] = $request->permission_ids ?? [];
        $role = $this->roleRepository->create($dataCreate);
        $role->assignPermissions($dataCreate['permission_ids']);
        return $role;
    }

    public function findById($id)
    {
        return $this->roleRepository->findById($id);
    }

    public function update($request, $id)
    {
        $role = $this->roleRepository->findById($id);
        $dataUpdate = $request->all();
        $dataUpdate['permission_ids'] = $request->permission_ids ?? [];
        $role->update($dataUpdate);
        $role->syncPermissions($dataUpdate['permission_ids']);
        return $role;
    }

    public function delete($id)
    {
        $role = $this->roleRepository->findById($id);
        $role->delete();
        return $role;
    }
}
