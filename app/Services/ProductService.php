<?php

namespace App\Services;

use App\Repositories\ProductRepository;
use App\Traits\HandleImage;

class ProductService
{
    use HandleImage;

    protected $productRepository;

    public function __construct(ProductRepository $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    public function count()
    {
        return $this->productRepository->count();
    }

    public function search($request)
    {
        $dataSearch = $request->all();
        $dataSearch['name'] = $request->name ?? '';
        $dataSearch['category_id'] = $request->category_id ?? '';
        $dataSearch['min_price'] = $request->min_price ?? '';
        $dataSearch['max_price'] = $request->max_price ?? '';
        return $this->productRepository->search($dataSearch)->appends($request->all());
    }

    public function findById($id)
    {
        return $this->productRepository->findById($id);
    }

    public function create($request)
    {
        $dataCreate = $request->all();
        $dataCreate['category_ids'] = $request->category_ids ?? [];
        $product = $this->productRepository->create($dataCreate);
        $product->syncCategories($dataCreate['category_ids']);
        $imageName = $this->saveImage($request);
        $product->update(['image' => $imageName]);
        return $product;
    }

    public function update($request, $id)
    {
        $product = $this->productRepository->findById($id);
        $dataUpdate = [
            'name' => $request->name,
            'description' => $request->description,
            'price' => $request->price,
        ];
        $product->update($dataUpdate);
        $dataUpdate['category_ids'] = $request->category_ids ?? [];
        $product->syncCategories($dataUpdate['category_ids']);
        $imageName = $this->updateImage($request, $product->image);
        $product->update(['image' => $imageName]);
        return $product;
    }

    public function delete($id)
    {
        $product = $this->productRepository->findById($id);
        $product->delete();
        $this->deleteImage($product->image);
        return $product;
    }
}
