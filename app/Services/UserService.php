<?php

namespace App\Services;

use App\Repositories\UserRepository;
use Illuminate\Support\Facades\Hash;

class UserService
{
    protected $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function count()
    {
        return $this->userRepository->count();
    }

    public function search($request)
    {
        $dataSearch = $request->all();
        $dataSearch['email'] = $request->email ?? '';
        $dataSearch['role_id'] = $request->role_id ?? '';
        return $this->userRepository->search($dataSearch)->appends($request->all());
    }

    public function create($request)
    {
        $dataCreate = $request->all();
        $dataCreate['password'] = Hash::make($dataCreate['password']);
        $dataCreate['role_ids'] = $request->role_ids ?? [];
        $user = $this->userRepository->create($dataCreate);
        $user->assignRoles($dataCreate['role_ids']);
        return $user;
    }

    public function findById($id)
    {
        return $this->userRepository->findById($id);
    }

    public function update($request, $id)
    {
        $dataUpdate = $request->all();
        $user = $this->userRepository->findById($id);
        $dataUpdate['role_ids'] = $request->role_ids ?? [];
        $user->update($dataUpdate);
        $user->syncRoles($dataUpdate['role_ids']);
        return $user;
    }

    public function delete($id)
    {
        $user = $this->userRepository->findById($id);
        $user->delete();
        return $user;
    }
}
