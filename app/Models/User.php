<?php

namespace App\Models;

use App\Traits\HandlePermission;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Auth;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable, HandlePermission;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function scopeWithEmail($query, $email)
    {
        return $email ? $query->where('email', 'LIKE', "%{$email}%") : null;
    }

    public function scopeWithRoleId($query, $roleId)
    {
        return $roleId ? $query->whereHas('roles', function ($q) use ($roleId) {
            $q->where('id', $roleId);
        }) : null;
    }

    public function roles()
    {
        return $this->belongsToMany(Role::class, 'user_role', 'user_id', 'role_id')
            ->withTimestamps();
    }

    public function assignRoles($roleIds)
    {
        return $this->roles()->attach($roleIds);
    }

    public function syncRoles($roleIds)
    {
        return $this->roles()->sync($roleIds);
    }

    /*public function hasPermission($permission){
        if($this->roles->contains('name','super-admin')){
            return true;
        } else {
            $per = Permission::where('name','=',$permission)->first();
            return $this->hasAnyRoles($per->roles);
        }
    }

    public function hasAnyRoles($roles){
        $result = array_intersect($roles->pluck('name')->toArray(),$this->roles->pluck('name')->toArray());
        if(count($result) > 0) {
            return true;
        } else {
            return false;
        }
    }*/
}
