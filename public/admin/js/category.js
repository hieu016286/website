getList();
getParentCategories();

function callCategoryApi(url, data = {}, method = "GET") {
    return $.ajax({
        type: method,
        url: url,
        data: data,
    })
}

function getList(page) {
    let url = $('#list').data('action') + "?page=" + page;
    let name = $('#name').val();
    let parentId = $('#parentId').val();
    let data = {
        name: name,
        parent_id: parentId,
    }
    callCategoryApi(url, data)
        .then(function (res) {
            $('#list').replaceWith(res);
        })
}

function afterCallApiSuccess(res) {
    getList();
    getParentCategories();
    $('#success_message').show();
    $('#success_message').addClass('alert alert-success');
    $('#success_message').text(res.message);
    $('#success_message').hide(3000);
}

$(document).on('click', '.pagination a', function (e) {
    e.preventDefault();
    let page = $(this).attr('href').split('page=')[1];
    getList(page);
})
$('#name').on('keyup', function () {
    getList();
})
$('#parentId').on('change', function () {
    $('#currentParentId').val($(this).val());
    getList();
})

function getParentCategories() {
    let url = $('#parentId').data('action');
    callCategoryApi(url)
        .then(function (res) {
            $('#parentIdCreate').html('<option value=" " selected>Open this select menu</option>')
            $.each(res.parentCategories, function (k, v) {
                $('#parentIdCreate').append('<option value="' + v.id + '">' + v.name + '</option>')
            });
            $('#parentId').html('<option value=" " selected>Select Main Category</option>')
            $.each(res.parentCategories, function (k, v) {
                $('#parentId').append('<option value="' + v.id + '">' + v.name + '</option>')
                if (v.id == $('#currentParentId').val()) {
                    $('#parentId option[value="' + v.id + '"]').prop('selected', true);
                }
            });
        })
}

$('.createCategory').on('click', function (e) {
    e.preventDefault();
    let url = $('.createCategory').data('action');
    let data = {
        name: $('#nameCreate').val(),
        parent_id: $('#parentIdCreate').val(),
    };
    let method = "POST";
    callCategoryApi(url, data, method)
        .then(function (res) {
            if (res.status == 400) {
                $.each(res.errors, function (k, v) {
                    $('span.' + k + '_error_create').text(v[0]);
                })
            } else {
                afterCallApiSuccess(res);
                $('#CreateCategoryModal').modal('hide');
                $('#CreateCategoryModal').find('input').val("");
            }
        })
})

function editCategory(id) {
    $('#EditCategoryModal').modal('show');
    let url = `/categories/${id}/edit`;
    let data = {}
    let method = "GET";
    callCategoryApi(url, data, method)
        .then(function (res) {
            $('#cID').val(res.category.id);
            $('#nameUpdate').val(res.category.name);
            $('#parentIdUpdate').html('<option value=" " selected>Open this select menu</option>')
            $.each(res.parentCategories, function (k, v) {
                $('#parentIdUpdate').append('<option value="' + v.id + '" >' + v.name + '</option>')
                if (res.category.parent_id == v.id) {
                    $('#parentIdUpdate option[value="' + v.id + '"]').prop('selected', true);
                }
                if (res.category.id == v.id) {
                    $('#parentIdUpdate option[value="' + v.id + '"]').prop('disabled', true);
                }
            })
        })
}

$('.updateCategory').on('click', function (e) {
    e.preventDefault();
    let cID = $('#cID').val();
    let url = `/categories/${cID}`;
    let data = {
        name: $('#nameUpdate').val(),
        parent_id: $('#parentIdUpdate').val(),
    };
    let method = "PUT";
    callCategoryApi(url, data, method)
        .then(function (res) {
            if (res.status == 400) {
                $.each(res.errors, function (k, v) {
                    $('span.' + k + '_error_update').text(v[0]);
                })
            } else {
                afterCallApiSuccess(res);
                $('#EditCategoryModal').modal('hide');
                $('#EditCategoryModal').find('input').val("");
            }
        })
})

function deleteCategory(id) {
    Swal.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
        if (result.isConfirmed) {
            let url = `/categories/${id}`;
            let data = {};
            let method = "DELETE";
            callCategoryApi(url, data, method)
                .then(function () {
                    getList();
                    getParentCategories();
                })
            Swal.fire(
                'Deleted!',
                'Your file has been deleted.',
                'success'
            )
        }
    })
}

$('#CreateCategoryModal').on('hide.bs.modal', function (e) {
    $(this).find("input").val('').end()
        .find("span").text('').end()
        .find("option[value='0']").prop("selected", true).end();
})
$('#EditCategoryModal').on('hide.bs.modal', function (e) {
    $(this).find("input").val('').end()
        .find("span").text('').end()
        .find("option[value='0']").prop("selected", true).end();
})
