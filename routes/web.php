<?php

use App\Http\Controllers\ProductController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::group(['as' => 'users.', 'middleware' => 'auth'], function () {
    Route::get('users', [UserController::class, 'index'])
        ->name('index')
        ->middleware('permission:index-user');
    Route::get('users/{user}', [UserController::class, 'show'])
        ->name('show')
        ->where('user', '[0-9]+')
        ->middleware('permission:index-user');
    Route::get('/users/search/user', [UserController::class, 'search'])
        ->name('search')
        ->middleware('permission:index-user');;
    Route::get('users/create', [UserController::class, 'create'])
        ->name('create')
        ->middleware('permission:create-user');
    Route::post('users', [UserController::class, 'store'])
        ->name('store')
        ->middleware('permission:create-user');
    Route::get('users/{user}/edit', [UserController::class, 'edit'])
        ->name('edit')
        ->middleware(['permission:edit-user', 'check.user']);
    Route::put('users/{user}', [UserController::class, 'update'])
        ->name('update')
        ->middleware(['permission:edit-user', 'check.user']);
    Route::delete('users/{user}', [UserController::class, 'destroy'])
        ->name('destroy')
        ->middleware(['permission:delete-user', 'check.user']);
});

Route::group(['as' => 'roles.', 'middleware' => 'auth'], function () {
    Route::get('roles', [RoleController::class, 'index'])
        ->name('index')
        ->middleware('permission:index-role');
    Route::get('roles/{role}', [RoleController::class, 'show'])
        ->name('show')
        ->where('role', '[0-9]+')
        ->middleware('permission:index-role');
    Route::get('/roles/search/role', [RoleController::class, 'search'])
        ->name('search')
        ->middleware('permission:index-role');
    Route::get('roles/create', [RoleController::class, 'create'])
        ->name('create')
        ->middleware('permission:create-role');
    Route::post('roles', [RoleController::class, 'store'])
        ->name('store')
        ->middleware('permission:create-role');
    Route::get('roles/{role}/edit', [RoleController::class, 'edit'])
        ->name('edit')
        ->middleware(['permission:edit-role', 'canNotModifyRoleSuperAdmin']);
    Route::put('roles/{role}', [RoleController::class, 'update'])
        ->name('update')
        ->middleware(['permission:edit-role', 'canNotModifyRoleSuperAdmin']);
    Route::delete('roles/{role}', [RoleController::class, 'destroy'])
        ->name('destroy')
        ->middleware(['permission:delete-role', 'canNotModifyRoleSuperAdmin']);
});

Route::group(['as' => 'categories.', 'middleware' => 'auth', 'prefix' => 'categories'], function () {
    Route::get('/', [CategoryController::class, 'index'])
        ->name('index')
        ->middleware('permission:index-category');
    Route::get('/list', [CategoryController::class, 'list'])
        ->name('list')
        ->middleware('permission:index-category');
    Route::get('/getParentCategories', [CategoryController::class, 'getParentCategories'])
        ->name('getParentCategories')
        ->middleware('permission:index-category');
    Route::post('/', [CategoryController::class, 'store'])
        ->name('store')
        ->middleware('permission:create-category');
    Route::get('/{category}/edit', [CategoryController::class, 'edit'])
        ->name('edit')
        ->middleware('permission:edit-category');
    Route::put('/{category}', [CategoryController::class, 'update'])
        ->name('update')
        ->middleware('permission:edit-category');
    Route::delete('/{category}', [CategoryController::class, 'destroy'])
        ->name('destroy')
        ->middleware('permission:delete-category');
});

Route::group(['as' => 'products.', 'middleware' => 'auth'], function () {
    Route::get('products', [ProductController::class, 'index'])
        ->name('index')
        ->middleware('permission:index-product');
    Route::get('products/{product}', [ProductController::class, 'show'])
        ->name('show')
        ->where('product', '[0-9]+')
        ->middleware('permission:index-product');
    Route::get('/products/search/product', [ProductController::class, 'search'])
        ->name('search')
        ->middleware('permission:index-product');
    Route::get('products/create', [ProductController::class, 'create'])
        ->name('create')
        ->middleware('permission:create-product');
    Route::post('products', [ProductController::class, 'store'])
        ->name('store')
        ->middleware('permission:create-product');
    Route::get('products/{product}/edit', [ProductController::class, 'edit'])
        ->name('edit')
        ->middleware('permission:edit-product');
    Route::put('products/{product}', [ProductController::class, 'update'])
        ->name('update')
        ->middleware('permission:edit-product');
    Route::delete('products/{product}', [ProductController::class, 'destroy'])
        ->name('destroy')
        ->middleware('permission:delete-product');
});

