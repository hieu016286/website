<?php

namespace Tests\Feature\Product;

use App\Models\Category;
use App\Models\Product;
use Illuminate\Http\Response;
use Tests\TestCase;

class SearchProductTest extends TestCase
{
    /** @test */
    public function super_admin_can_search_products_follow_category_and_follow_name_and_follow_price()
    {
        $this->loginWithSuperAdmin();
        $product = Product::factory()->create();
        $category = Category::factory()->create()->pluck('id');
        $category_search = $product->categories()->attach($category);
        $response = $this->get('/products?category_id=' . $category_search
            . '&name=' . $product->name . ''
            . '&min_price' . $product->min_price . ''
            . '&max_price' . $product->max_price . ''
        );
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('admin.products.index');
        $response->assertSee($product->name);
    }

    /** @test */
    public function authenticated_user_have_permission_can_search_product_follow_categories_and_name_and_price()
    {
        $this->loginUserWithPermission('index-product');
        $product = Product::factory()->create();
        $category = Category::factory()->create()->pluck('id');
        $category_search = $product->categories()->attach($category);
        $response = $this->get('/products?category_id=' . $category_search
            . '&name=' . $product->name . ''
            . '&min_price' . $product->min_price . ''
            . '&max_price' . $product->max_price . ''
        );
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('admin.products.index');
        $response->assertSee($product->name);
    }

    /** @test */
    public function authenticated_user_not_have_permission_can_not_search_product_follow_categories_and_name_and_price()
    {
        $this->loginWithUser();
        $product = Product::factory()->create();
        $category = Category::factory()->create()->pluck('id');
        $category_search = $product->categories()->attach($category);
        $response = $this->get('/products?category_id=' . $category_search
            . '&name=' . $product->name . ''
            . '&min_price' . $product->min_price . ''
            . '&max_price' . $product->max_price . ''
        );
        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }

    /** @test */
    public function unauthenticated_user_can_not_search_product_follow_category_name_and_price()
    {
        $product = Product::factory()->create();
        $category = Category::factory()->create()->pluck('id');
        $category_search = $product->categories()->attach($category);
        $response = $this->get('/products?category_id=' . $category_search
            . '&name=' . $product->name . ''
            . '&min_price' . $product->min_price . ''
            . '&max_price' . $product->max_price . ''
        );
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/login');
    }
}
