<?php

namespace Tests\Feature\Role;

use App\Models\Role;
use Illuminate\Http\Response;
use Tests\TestCase;

class CreateRoleTest extends TestCase
{
    /** @test */
    public function super_admin_can_see_create_role_form()
    {
        $this->loginWithSuperAdmin();
        $response = $this->get(route('roles.create'));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('admin.roles.create');
        $response->assertSee('name')->assertSee('display_name');
    }

    /** @test */
    public function super_admin_can_create_new_role()
    {
        $this->loginWithSuperAdmin();
        $role = Role::factory()->make()->toArray();
        $response = $this->post(route('roles.store'), $role);
        $response->assertStatus(Response::HTTP_FOUND);
        $this->assertDatabaseHas('roles', $role);
        $response->assertRedirect(route('roles.index'));
    }

    /** @test */
    public function super_admin_can_not_create_new_role_if_name_and_display_name_are_null()
    {
        $this->loginWithSuperAdmin();
        $role = Role::factory([
            'name' => null,
            'display_name' => null,
        ])->make()->toArray();
        $response = $this->post(route('roles.store'), $role);
        $response->assertSessionHasErrors(['name', 'display_name']);
    }

    /** @test */
    public function authenticated_user_not_have_permission_can_not_see_create_role_form()
    {
        $this->loginWithUser();
        $response = $this->get(route('roles.create'));
        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }

    /** @test */
    public function authenticated_user_not_have_permission_can_not_create_new_role()
    {
        $this->loginWithUser();
        $role = Role::factory()->make()->toArray();
        $response = $this->post(route('roles.store'), $role);
        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }

    /** @test */
    public function authenticated_user_have_permission_can_see_create_role_form()
    {
        $this->loginUserWithPermission('create-role');
        $response = $this->get(route('roles.create'));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('admin.roles.create');
        $response->assertSee('name')->assertSee('display_name');
    }

    /** @test */
    public function authenticated_user_have_permission_can_create_new_role()
    {
        $this->loginUserWithPermission('create-role');
        $role = Role::factory()->make()->toArray();
        $response = $this->post(route('roles.store'), $role);
        $response->assertStatus(Response::HTTP_FOUND);
        $this->assertDatabaseHas('roles', $role);
        $response->assertRedirect(route('roles.index'));
    }

    /** @test */
    public function authenticated_user_have_permission_can_not_create_new_role_if_name_and_display_name_are_null()
    {
        $this->loginUserWithPermission('create-role');
        $role = Role::factory([
            'name' => null,
            'display_name' => null,
        ])->make()->toArray();
        $response = $this->post(route('roles.store'), $role);
        $response->assertSessionHasErrors(['name', 'display_name']);
    }

    /** @test */
    public function unauthenticated_user_can_not_see_create_form()
    {
        $response = $this->get(route('roles.create'));
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/login');
    }

    /** @test */
    public function unauthenticated_user_can_not_create_role()
    {
        $role = Role::factory()->make()->toArray();
        $response = $this->post(route('roles.store'), $role);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/login');
        $this->assertDatabaseMissing('roles', $role);
    }
}
