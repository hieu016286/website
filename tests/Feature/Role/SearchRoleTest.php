<?php

namespace Tests\Feature\Role;

use App\Models\Role;
use Illuminate\Http\Response;
use Tests\TestCase;

class SearchRoleTest extends TestCase
{
    /** @test */
    public function super_admin_can_search_display_name_role()
    {
        $this->loginWithSuperAdmin();
        $role = Role::factory()->create();
        $response = $this->get('/roles?name=' . $role->display_name . '');
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('admin.roles.index');
        $response->assertSee($role->display_name);
    }

    /** @test */
    public function authenticated_user_can_not_search_display_name_role()
    {
        $this->loginWithUser();
        $role = Role::factory()->create();
        $response = $this->get('/roles?name=' . $role->display_name . '');
        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }

    /** @test */
    public function authenticated_user_have_permission_can_search_display_name_role()
    {
        $this->loginUserWithPermission('index-role');
        $role = Role::factory()->create();
        $response = $this->get('/roles?name=' . $role->display_name . '');
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('admin.roles.index');
        $response->assertSee($role->display_name);
    }

    /** @test */
    public function unauthenticated_user_can_not_search_display_name_role()
    {
        $role = Role::factory()->create();
        $response = $this->get('/roles?name=' . $role->display_name . '');
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/login');
    }
}
