<?php

namespace Tests\Feature\User;

use App\Models\User;
use Illuminate\Http\Response;
use Tests\TestCase;

class DeleteUserTest extends TestCase
{
    /** @test */
    public function super_admin_can_delete_user()
    {
        $this->loginWithSuperAdmin();
        $user = User::factory()->create();
        $response = $this->delete(route('users.destroy', $user->id));
        $this->assertDatabaseMissing('users', $user->toArray());
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect(route('users.index'));
    }

    /** @test */
    public function super_admin_can_not_delete_user_super_admin()
    {
        $this->loginWithSuperAdmin();
        $user = User::where('name', 'Mr.A')->first();
        $response = $this->delete(route('users.destroy', $user->id));
        $response->assertStatus(Response::HTTP_NOT_FOUND);
        $this->assertDatabaseHas('users', $user->toArray());
    }

    /** @test */
    public function authenticated_user_have_permission_can_delete_user()
    {
        $this->loginUserWithPermission('delete-user');
        $user = User::factory()->create();
        $response = $this->delete(route('users.destroy', $user->id));
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect(route('users.index'));
    }

    /** @test */
    public function authenticated_user_not_have_permission_can_not_delete_user()
    {
        $this->loginWithUser();
        $user = User::factory()->create();
        $response = $this->delete(route('users.destroy', $user->id));
        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }

    /** @test */
    public function authenticated_user_have_permission_can_not_delete_user_super_admin()
    {
        $this->loginUserWithPermission('delete-user');
        $user = User::where('name', 'Mr.A')->first();
        $response = $this->delete(route('users.destroy', $user->id));
        $response->assertStatus(Response::HTTP_NOT_FOUND);
        $this->assertDatabaseHas('users', $user->toArray());
    }

    /** @test */
    public function unauthenticated_user_can_not_delete_user()
    {
        $user = User::factory()->create();
        $response = $this->delete(route('users.destroy', $user));
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/login');
    }
}
