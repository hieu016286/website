<?php

namespace Tests\Feature\Category;

use App\Models\Category;
use Illuminate\Http\Response;
use Tests\TestCase;

class SearchCategoryTest extends TestCase
{
    /** @test */
    public function super_admin_can_search_all_categories()
    {
        $this->loginWithSuperAdmin();
        $category = Category::factory()->create();
        $response = $this->get('categories/list?name=' . $category->name . '&parent_id=' . $category->parent_id . '');

        $response->assertStatus(Response::HTTP_OK);
        $response->assertSee($category->name);
    }

    /** @test */
    public function authenticated_user_can_not_search_all_categories()
    {
        $this->loginWithUser();
        $category = Category::factory()->create();
        $response = $this->get('categories/list?name=' . $category->name . '&parent_id=' . $category->parent_id . '');

        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }

    /** @test */
    public function authenticated_user_has_permission_can_search_all_categories()
    {
        $this->loginUserWithPermission('index-category');
        $category = Category::factory()->create();
        $response = $this->get('categories/list?name=' . $category->name . '&parent_id=' . $category->parent_id . '');

        $response->assertStatus(Response::HTTP_OK);
        $response->assertSee($category->name);
    }

    /** @test */
    public function unauthenticated_user_can_not_search_all_categories()
    {
        $category = Category::factory()->create();
        $response = $this->get('categories/list?name=' . $category->name . '&parent_id=' . $category->parent_id . '');

        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/login');
    }
}
