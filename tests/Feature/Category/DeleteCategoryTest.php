<?php

namespace Tests\Feature\Category;

use App\Models\Category;
use Illuminate\Http\Response;
use Tests\TestCase;

class DeleteCategoryTest extends TestCase
{
    /** @test */
    public function super_admin_can_delete_category()
    {
        $this->loginWithSuperAdmin();
        $category = Category::factory()->create()->toArray();
        $response = $this->delete(route('categories.destroy', $category['id']));

        $response->assertStatus(Response::HTTP_OK);
        $this->assertDatabaseMissing('categories', ['name' => $category['name']]);
    }

    /** @test */
    public function authenticated_user_can_not_delete_category()
    {
        $this->loginWithUser();
        $category = Category::factory()->create()->toArray();
        $response = $this->delete(route('categories.destroy', $category['id']));

        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }

    /** @test */
    public function authenticated_user_have_permission_can_delete_category()
    {
        $this->loginUserWithPermission('delete-category');
        $category = Category::factory()->create()->toArray();
        $response = $this->delete(route('categories.destroy', $category['id']));

        $response->assertStatus(Response::HTTP_OK);
        $this->assertDatabaseMissing('categories', ['name' => $category['name']]);
    }

    /** @test */
    public function unauthenticated_user_can_not_delete_category()
    {
        $category = Category::factory()->create()->toArray();
        $response = $this->delete(route('categories.destroy', $category['id']));

        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/login');
    }
}
