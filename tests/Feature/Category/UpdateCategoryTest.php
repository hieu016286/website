<?php

namespace Tests\Feature\Category;

use App\Models\Category;
use Illuminate\Http\Response;
use Illuminate\Support\Str;
use Tests\TestCase;

class UpdateCategoryTest extends TestCase
{
    /** @test */
    public function super_admin_can_edit_category_form()
    {
        $this->loginWithSuperAdmin();
        $category = Category::factory()->create()->toArray();
        $response = $this->get(route('categories.edit', $category['id']));

        $response->assertStatus(Response::HTTP_OK);
        $response->assertJsonFragment(['category' => $category]);
    }

    /** @test */
    public function super_admin_can_update_category()
    {
        $this->loginWithSuperAdmin();
        $category = Category::factory()->create()->toArray();
        $name = Str::random(10);
        $data = [
            'name' => $name,
            'categories_id' => $this->getParentCategoryId(),
        ];
        $response = $this->put(route('categories.update', $category['id']), $data);

        $response->assertStatus(Response::HTTP_OK);
        $response->assertJsonFragment(['message' => 'Update Category Success !!!']);
        $this->assertDatabaseHas('categories', ['name' => $name]);
    }

    /** @test */
    public function super_admin_can_not_update_category_if_name_is_null()
    {
        $this->loginWithSuperAdmin();
        $category = Category::factory()->create()->toArray();
        $name = Str::random(10);
        $data = [
            'name' => null,
            'categories_id' => $this->getParentCategoryId(),
        ];
        $response = $this->put(route('categories.update', $category['id']), $data);

        $response->assertStatus(Response::HTTP_OK);
        $response->assertJsonValidationErrors(['name']);
    }

    /** @test */
    public function authenticated_user_can_not_update_category()
    {
        $this->loginWithUser();
        $category = Category::factory()->create()->toArray();
        $name = Str::random(10);
        $data = [
            'name' => $name,
            'categories_id' => $this->getParentCategoryId(),
        ];
        $response = $this->put(route('categories.update', $category['id']), $data);

        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }

    /** @test */
    public function authenticated_user_has_permission_can_update_category()
    {
        $this->loginUserWithPermission('edit-category');
        $category = Category::factory()->create()->toArray();
        $name = Str::random(10);
        $data = [
            'name' => $name,
            'categories_id' => $this->getParentCategoryId(),
        ];
        $response = $this->put(route('categories.update', $category['id']), $data);

        $response->assertStatus(Response::HTTP_OK);
        $response->assertJsonFragment(['message' => 'Update Category Success !!!']);
        $this->assertDatabaseHas('categories', ['name' => $name]);
    }

    /** @test */
    public function authenticated_user_have_permission_not_update_category_if_name_is_null()
    {
        $this->loginWithSuperAdmin();
        $category = Category::factory()->create()->toArray();
        $name = Str::random(10);
        $data = [
            'name' => null,
            'categories_id' => $this->getParentCategoryId(),
        ];
        $response = $this->put(route('categories.update', $category['id']), $data);

        $response->assertStatus(Response::HTTP_OK);
        $response->assertJsonValidationErrors(['name']);
    }

    /** @test */
    public function unauthenticated_user_can_not_update_category()
    {
        $category = Category::factory()->create()->toArray();
        $name = Str::random(10);
        $data = [
            'name' => $name,
            'categories_id' => $this->getParentCategoryId(),
        ];
        $response = $this->put(route('categories.update', $category['id']), $data);

        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/login');
    }

}
